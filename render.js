function renderBooks(booksContainer, books) {
    booksContainer.empty();
    books.forEach(book => renderNewBook(booksContainer, book, books));
}

function renderNewBook(booksContainer, book, books) {
    const index = books.map(b => b.id).indexOf(book.id);
    const newItem = renderTemplate('book-item-template', book);
    switch (index) {
        case -1:
            break;
        case 0:
            booksContainer.prepend(newItem);
            break;
        case books.length - 1:
            booksContainer.append(newItem);
            break;
        default:
            booksContainer.find(`#book-${books[index - 1].id}`).after(newItem);
    }
}

function eraseRemovedBook(booksContainer, bookId) {
    const targetElement = booksContainer.find(`#book-${bookId}`);
    if(targetElement)
        targetElement.remove();
}

function renderOldBook(booksContainer, book) {
    booksContainer.find(`#book-${book.id}`).replaceWith(renderTemplate('book-item-template', book));
}

function renderTemplate(name, data) {
    let template = document.getElementById(name).innerHTML;

    for (let property in data) {
        if (data.hasOwnProperty(property)) {
            let search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }
    return template;
}