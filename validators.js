function addError(selector, message) {
    if (selector.hasClass('validate-error'))
        return;

    selector.addClass('validate-error');
    selector.closest('p').append(`<p class='error-msg'>${message}</p>`);
}

function removeError(selector) {
    if (!selector.hasClass('validate-error'))
        return;
    selector.removeClass('validate-error');
    selector.closest('p').find(`p.error-msg`).remove();
}

function validateTextField(selector) {
    if (!selector.val())
        addError(selector, "Данное поле не должно быть пустым");
    else
        removeError(selector)
}

function validateNumberField(selector) {
    const val = selector.val();
    if (val === "" || !(new RegExp('^[0-9]*$')).test(val)) {
        addError(selector, "В данное поле должно быть записано целое число");
        return;
    }

    if (parseInt(val) <= 0) {
        addError(selector, "Значение должно быть больше нуля");
        return;
    }

    removeError(selector);
}
