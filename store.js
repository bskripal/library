class Store {
    constructor() {
        if (!localStorage.hasOwnProperty('books'))
            localStorage.setItem('books', "[]");
        if (!localStorage.hasOwnProperty('booksCounter'))
            localStorage.setItem('booksCounter', '0');
        if (!localStorage.hasOwnProperty('order'))
            localStorage.setItem('order', 'default');
        if (!localStorage.hasOwnProperty('filter'))
            localStorage.setItem('filter', '');

        this.getOrderedBook = (books, order) => {
            return books.sort(function (a, b) {
                const fieldName = order;
                if (a[fieldName] > b[fieldName])
                    return 1;
                if (a[fieldName] > b[fieldName])
                    return -1;
                return 0;
            })
        };

        this.getFilteredBook = (books, filter) => {
            const filterStr = filter.trim().toLowerCase();
            if (!filterStr)
                return books;
            return books.filter(book => book.name.toLowerCase().indexOf(filterStr) !== -1);
        };

        this.addNewItem = (name, author, year, pages, id) => {
            const books = this.books;
            let newItem;

            if (id) {
                newItem = books.find(book => book.id === id);
                Object.assign(newItem, {name, author, year, pages});
            } else {
                const currentCounter = this.counter + 1;

                newItem = {
                    id: currentCounter,
                    name,
                    author,
                    year,
                    pages
                };
                books.push(newItem);
                this.counter = currentCounter;
            }
            this.books = books;
            return newItem;
        };

        this.removeItem = (id) => {
            const r = confirm("Вы действительно хотите удалить книгу?");
            if (r) {
                this.books = this.books.filter(book => book.id !== id);
                return true;
            }
            return false;
        };

        this.getBookById = (id) => this.books.find(book => book.id === id);
    }

    get books() {
        return JSON.parse(localStorage.getItem("books"));
    }

    set books(books) {
        localStorage.setItem('books', JSON.stringify(books));
    }

    get counter() {
        return parseInt(localStorage.getItem('booksCounter'));
    }

    set counter(counter) {
        localStorage.setItem('booksCounter', `${counter}`);
    }

    get order() {
        return localStorage.getItem('order');
    }

    set order(order) {
        localStorage.setItem('order', order);
    }

    get filter() {
        return localStorage.getItem('filter');
    }

    set filter(filter) {
        localStorage.setItem('filter', filter);
    }

    get allowedBooks() {
        return this.getOrderedBook(this.getFilteredBook(this.books, this.filter), this.order);
    }
}
